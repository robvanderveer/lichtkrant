/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

#include "Arduino.h"

/*
 * timedEvent class
 * - default timeout is 1 second. Specify timeout with constructor or override with hasElapsed(x)
 *
 * - reset(): reset this time
 * - hasElapsed(): returns true when the event should trigger
 * - hasElapsed(ULONG): override the timeout
 * - hasElapsed(ULONG, BOOL): override timeout and specify if the event should reset when triggered
 */

#ifndef TIMEDEVENT
#define TIMEDEVENT

struct timedEvent
{
    unsigned long duration;
    unsigned long lastEvent;

    bool hasElapsed()
    {
       return hasElapsed(duration, true);
    }

    //defaults to 1 second.
    timedEvent()
    {
       lastEvent = millis();
       duration = 1000;
    }

    bool hasElapsed(unsigned long t)
    {
      return hasElapsed(t, true);
    }

    bool hasElapsed(unsigned long t, bool autoReset)
    {
      bool elapsed = (millis() - lastEvent) > t;
      if(elapsed && autoReset)
      {
          reset(t);
      }
      return elapsed;
    }

    void reset()
    {
       lastEvent = millis();
    }

    void reset(unsigned long d)
    {
       duration = d;
       reset();
    }

    void fire()
    {
      lastEvent = -duration;
    }
};

#endif
