#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include "timedEvent.h"

class WifiConfigure
{
  private:
     std::unique_ptr<ESP8266WebServer> server;

  public:
     WifiConfigure()
     {
      
     }

     void connect(const char *ap, const char *apPassword)
     {
        //the arguments are used to define the config Access Point, not the SSID to use

        Serial.println(F("Try to connect with known credentials"));

        WiFi.mode(WIFI_STA);
        WiFi.begin();
        yield();
        if(hasConnection())
        {
          Serial.println(F("Connection established"));
          return;
        }

        //unable to connect, go to configure mode
        Serial.println("Needs configuration");
        configure(ap, apPassword);
     }

     bool hasConnection()
     {
        timedEvent timeout;
        while(!timeout.hasElapsed(5000))
        {
          int state = WiFi.status();
          if(state == WL_CONNECTED)
          {
            Serial.println(F("Connected"));       
            return true;
          }
          else
          {
            Serial.println(state);
          }
          delay(500);
        } 
        return false;
     }

     //set configure mode, generate a new Access Point with a private webservice to enter wifi credentials
     void configure(const char *ap, const char *apPassword)
     {
        Serial.println(F("Setting up configuration portal at 192.168.10.1"));
        
        WiFi.mode(WIFI_AP_STA);
        WiFi.softAPConfig(IPAddress(192,168,10,1), IPAddress(192,168,10,1), IPAddress(255,255,255,0));
        WiFi.softAP(ap, apPassword);

        delay(500);
        Serial.println(F("SoftAP set up"));
        Serial.println(WiFi.softAPIP());

        server.reset(new ESP8266WebServer(80));
        server->on("/", std::bind(&WifiConfigure::handleHomepage, this));
        server->begin();

        Serial.println(F("Waiting for HTTP Clients"));

        while(true)
        {
          server->handleClient();

          //optional, when the connection has been established, stop the webserver etc and continue normal ops.
          delay(5);
        }

        server.reset();

        Serial.println(F("Configuration finished."));
        WiFi.mode(WIFI_STA);
     }

     void sendContent(String title, String body)
     {
        String complete = "<html><body><h1>" + title + "</h1>" + body + "</body></html>";
        server->send(200, "text/html", complete);
     }

     void handleHomepage()
     {
        String ssid;
        String pass;

        Serial.println(F("Processing homepage"));

        if(server->hasArg("ssid") && server->hasArg("pass"))
        {
          ssid = server->arg("ssid");
          pass = server->arg("pass");

          Serial.println(F("Postback detected"));
          WiFi.mode(WIFI_STA); //<- WORKAROUND, SHOULD NOT BE THERE
          int result = WiFi.begin(ssid.c_str(), pass.c_str());
          Serial.println(ssid);
          Serial.println(pass);
          Serial.println(result);

          timedEvent timeout;
          while(!timeout.hasElapsed(20000))
          {
            if(WiFi.status() == WL_CONNECTED)
            {
              Serial.println(F("Connected"));       

              sendContent("Connected", "WiFi succesfully configured. Reboot to enter normal operation.");
              return;
            }
            else
            {
              if(WiFi.status() == WL_NO_SSID_AVAIL)
              {
                Serial.println(F("WL_NO_SSID_AVAIL"));
                break;       
              }
              else
              {
                Serial.println(WiFi.status());
              }
            }
            delay(100);
          } 
          Serial.println(F("Timeout while connecting"));       
        }

        //build the UI.
        String inputSSID = "<label>SSID</label><input type=\"text\" name=\"ssid\" value=\"" + ssid + "\"/><br>";
        String inputPASS = "<label>Password</label><input type=\"password\" name=\"pass\" value=\"" + pass + "\"/><br>";
        String inputSubmit ="<input type=\"submit\" value=\"Submit\">";

        String form = "<form action=\"/\" method=\"POST\">"
          + inputSSID
          + inputPASS
          + inputSubmit
          + "</form>";

        sendContent("Configure WiFi", form);
     }
};
    
