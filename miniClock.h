#ifndef MINICLOCK_H
#define MINICLOCK_H

#include <Time.h>


const byte Font3x5[] = {
  0b00011111, //0
  0b00010001,
  0b00011111,

  0b00000000, //1
  0b00000000,
  0b00011111,

  0b00011101, //2
  0b00010101,
  0b00010111,

  0b00010101, //3
  0b00010101,
  0b00011111,

  0b00000111, //4
  0b00000100,
  0b00011111,

  0b00010111, //5
  0b00010101,
  0b00011101,

  0b00011111, //6
  0b00010101,
  0b00011101,

  0b00000001, //7
  0b00000001,
  0b00011111,
  
  0b00011111, //8
  0b00010101,
  0b00011111,
  
  0b00010111, //9
  0b00010101,
  0b00011111,

  0b00000000, //:
  0b00001010,
  0b00000000
};


class MiniClock
{
  public:
    void drawTime(uint8_t *buffer)
    {
      int h = hour();
      int m = minute(); 
      int s = second();

      drawDigit(buffer, 1, h / 10);
      drawDigit(buffer, 5, h % 10);
      drawDigit(buffer, 8, 10);
      drawDigit(buffer, 11, m / 10);
      drawDigit(buffer, 15, m % 10);
      drawDigit(buffer, 18, 10);
      drawDigit(buffer, 21, s / 10);
      drawDigit(buffer, 25, s % 10);
    } 

    void drawDigit(uint8_t *buffer, uint8_t offset, uint8_t digit)
    {
      uint8_t charOffset = digit * 3;
      for(int i = 0; i < 3; i++)
      {
        buffer[offset + i] = Font3x5[charOffset+i] << 1;  //shift left for a little top margin
      }
    }
};

#endif //MINICLOCK_H
