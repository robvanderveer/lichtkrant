class MAX7219_Matrix 
{
  enum {
       MAX7219_REG_NOOP        = 0x0,
       MAX7219_REG_DECODEMODE  = 0x9,
       MAX7219_REG_INTENSITY   = 0xA,
       MAX7219_REG_SCANLIMIT   = 0xB,
       MAX7219_REG_SHUTDOWN    = 0xC,
       MAX7219_REG_DISPLAYTEST = 0xF,
      }; // end of enum


  private:
    byte _cs;
    byte _clk;
    byte _data;
    byte _numDisplays;

    void sendSPI(const uint8_t data)
    {
      digitalWrite(_clk, LOW);
      shiftOut(_data, _clk, MSBFIRST, data);
      digitalWrite(_clk, LOW);
    }

   public:
    MAX7219_Matrix(byte cs, byte clk, byte data, byte numdisplays)
    {
      _cs = cs;
      _clk = clk;
      _data = data;
      _numDisplays = numdisplays;
    }

    void send(const byte reg, const byte data)
    {
      digitalWrite(_cs, LOW);
      for(int d = 0; d < _numDisplays; d++)
      {
        sendSPI(reg);
        sendSPI(data);
      }
      digitalWrite(_cs, HIGH);
    }

    void begin()
    {
      Serial.println("Matrix begin");
      
      pinMode(_cs, OUTPUT);
      pinMode(_clk, OUTPUT);
      pinMode(_data, OUTPUT);

      digitalWrite(_cs, HIGH);
      digitalWrite(_clk, LOW);
      digitalWrite(_data, LOW);

      //todo initialize all displays
      send(MAX7219_REG_SCANLIMIT, 7);    //scanlimit
      send(MAX7219_REG_DECODEMODE, 0);    //decodemode
      send(MAX7219_REG_SHUTDOWN, 1);    //shutdown
      send(MAX7219_REG_DISPLAYTEST, 0);    //no test
      send(MAX7219_REG_INTENSITY, 0x02);   //intensity      
    }

    //refreshes the entire matrix in one go. 
    void blit(uint8_t *bitmap)
    {
      // Every byte is one column Every 8 bytes is one 8x8 matrix.
      for(int row = 0; row < 8; row++)
      {
        digitalWrite(_cs, LOW);
        
        for(int d = 0; d < _numDisplays; d++)
        {
          //collect bits. most-right segment first.
          uint8_t data = 0;
          for(int x=0; x< 8; x++)
          {
              bool isOn = bitRead(bitmap[x + d*8], row);
              bitWrite(data, 7-x, isOn);
          }
        
          sendSPI(row+1);
          sendSPI(data);
        }
        digitalWrite(_cs, HIGH);
      }
    }

    void clear()
    {
      for(int row = 0; row < 8; row++)
      {
        digitalWrite(_cs, LOW);
        
        for(int d = 0; d < _numDisplays; d++)
        {
          sendSPI(row+1);
          sendSPI(0);
        }
        digitalWrite(_cs, HIGH);
      }
    }
};


