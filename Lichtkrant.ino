#include <SPI.h>
#include <ESP8266WiFi.h>
//#include <ESP8266WebServer.h>
//#include <ESP8266mDNS.h>
#include <NtpClientLib.h>
#include <TimeLib.h> 
#include <PubSubClient.h>

#include "font_5x7.h"
#include "max7219.h"
#include "timedEvent.h"
#include "wifiConfig.h"
#include "miniClock.h"

const uint8_t PIN_CS = D2;
const uint8_t PIN_CLK = D5;
const uint8_t PIN_DATA = D7;

const uint8_t COLUMNS = 64;
uint8_t columnBuffer[COLUMNS];

void writePixel(int x, int y, bool value)
{
  bitWrite(columnBuffer[x], y, value);
}

int writeCharacter(char c, int pos, int offset)
{
  t_index index = Font5x7_idx[c];
  for(int i = 0; i <= index.len; i++)
  {
    int column = i + pos - offset;
    if(column >= 0 && column < COLUMNS)
    {
      uint8_t data = 0;
      if(i < index.len)
      {
          data = 0xFE & Font5x7[index.offset+i];
      }
      columnBuffer[column] = data;
    }
  }
  return index.len;
}

int measure(const char *msg)
{
  int pos = 0;
  while(*msg)
  {
    t_index index = Font5x7_idx[*msg++];
    pos += index.len;  
  }
  return pos;
}

void clearDisplay()
{
  for(uint8_t i = 0; i < COLUMNS; i++)
  {
     columnBuffer[i] = 0;
  }
}

void write(const char *msg, int offset)
{
  int pos = 0;
  while(*msg)
  {
    pos += writeCharacter(*msg++, pos, offset);  
  }
}

/* sketch start */


MAX7219_Matrix matrix(PIN_CS, PIN_CLK, PIN_DATA, 8);

int scrollOffset;
timedEvent scrollTimer;
timedEvent refreshTimer;
timedEvent blinkTimer;
timedEvent modeTimer;
unsigned long reconnectTimer;

bool blinkOnOff;

const unsigned int MAX_MESSAGE_SIZE = 256;

char messageBuffer[2][MAX_MESSAGE_SIZE];
int messageBufferIndex = 0;
bool messageWaiting = false;

//move these to config/FLASH
const char *MQTT_Topic = "sensorium/lichtkrant";
IPAddress MQTT_Server(192,168,2,3);
byte mac[]    = {  0xDE, 0xED, 0xBA, 0xFE, 0xFE, 0xED };

const int SCROLL_SPEED = 75;

WiFiClient wifiClient;
PubSubClient client(wifiClient);

MiniClock myClock;

enum DisplayMode 
{
  Clock,
  Message
} displayMode;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("Booting");

  matrix.begin();
  write("Connecting", 0);
  matrix.blit(columnBuffer);
  
  //init for now
  WifiConfigure wifiConfig;
  wifiConfig.connect("setuplichtkrant", "password");
  
  Serial.println ( "Connection assumed");

  Serial.println ( WiFi.localIP() );
  if ( MDNS.begin ( "lichtkrant" ) ) 
  {
    Serial.println ( "MDNS responder started" );
  }

  NTP.begin("pool.ntp.org", 1, true);
  NTP.setInterval(63);
  

  clearDisplay();
  matrix.clear();
  delay(500);

  messageBufferIndex = 0;
  scrollOffset = -COLUMNS;

  strcpy(messageBuffer[messageBufferIndex], "Waiting for messages...");

  initMQTT();

  displayMode = Clock;
}


void loop()
{
  loopMQTT();

  if(messageWaiting)
  {
    if( blinkTimer.hasElapsed(400))
    {
      blinkOnOff = !blinkOnOff;
    }
  }
  else
  {
    blinkOnOff = false;
  }

  switch(displayMode)
  {
    case Clock:
      clearDisplay();
      myClock.drawTime(columnBuffer);
//      matrix.blit(columnBuffer);

      if(modeTimer.hasElapsed(20*1000))
      {
        displayMode = Message;
        clearDisplay();
      }
      break;
    case Message:
      updateScrollDisplay();
      break;
  }

    //overlay status bits.
    writePixel(COLUMNS-1,7,blinkOnOff);
    writePixel(COLUMNS-2,7,client.connected() == false);
    matrix.blit(columnBuffer);


  delay(10);
}

void updateScrollDisplay()
{
    //update display
  if(scrollTimer.hasElapsed(SCROLL_SPEED))
  {
      write(messageBuffer[messageBufferIndex], scrollOffset);
      //matrix.blit(columnBuffer);
      
      scrollOffset++;
      if(scrollOffset > measure(messageBuffer[messageBufferIndex]))
      {
        Serial.println("End of message");
        //loop
        scrollOffset = -COLUMNS;

        if(messageWaiting)
        {
          Serial.println("Presenting new message");
          
          //swap the buffer.
          messageBufferIndex = 1 - messageBufferIndex;
          messageWaiting = false;
        }
        else
        {
          //back to clock
          displayMode = Clock;
          modeTimer.reset();
        }
      }
  }
}
/* mqtt stuff */

void initMQTT()
{
  client.setServer(MQTT_Server, 1883);
  client.setCallback(callbackMQTT);

  // Allow the hardware to sort itself out
  delay(1500);

  setupMQTT();
}

void setupMQTT()
{
    if(client.connect("lichtkrant"))
    {
      Serial.println("Connected!");
      
      client.subscribe(MQTT_Topic);
      client.publish("lichtkrant", "hello world");
    }
}

void loopMQTT()
{
  if(client.connected())
  {
    client.loop();
    reconnectTimer = millis();
  }
  else
  {
    //reconnect network.
    //keep trying every 2 seconds.
    if(millis() - reconnectTimer > 5*1000)
    {
      Serial.println("reconnecting");
      setupMQTT();

      reconnectTimer = millis();    
    }
  }
}

void callbackMQTT(char* topic, byte* payload, unsigned int length)
{
  Serial.print("Message arrived: ");
  Serial.print(topic);
  Serial.println();

  //todo: filter messages based on payload
  handleMQTTmessage(payload, length);
}

void handleMQTTmessage(byte* payload, unsigned int length)
{
  // fill message buffers.
  int buffer = 1-messageBufferIndex;
  
  for(int i = 0; i < length && i < MAX_MESSAGE_SIZE-1; i++)
  {
    messageBuffer[buffer][i] = (char)payload[i];
    messageBuffer[buffer][i+1] = 0;
  }
  Serial.print("New message: [");
  Serial.print(messageBuffer[buffer]);
  Serial.println("]");
  messageWaiting = true;
}

